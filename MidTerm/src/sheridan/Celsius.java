package sheridan;

public class Celsius {
	
	public static int convertFromFahrenheit(int degreeInFahrenheit) {
		int fahrenheit = degreeInFahrenheit;
		int celsius = (int)(Math.round(5.0/9.0*(fahrenheit-32)));
		return celsius;
	}

	public static void main(String[] args) {
		
		System.out.println("The result is " + convertFromFahrenheit(33));

	}

}
