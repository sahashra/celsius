package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CelsiusTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConvertFromFahrenheit() {
		int celsius = Celsius.convertFromFahrenheit(32);
		assertTrue("Unsuccessful conversion", celsius == 0);
	}
	@Test
	public void testConvertFromFahrenheitException() {
		int celsius = Celsius.convertFromFahrenheit(32);
		assertFalse("Unsuccessful conversion", celsius == 32.6);
	}
	@Test
	public void testConvertFromFahrenheitBoundaryIn() {
		int celsius = Celsius.convertFromFahrenheit(6);
		assertTrue("Unsuccessful conversion", celsius == Math.round(-14.49));
	}
	@Test
	public void testConvertFromFahrenheitBoundaryOut() {
		int celsius = Celsius.convertFromFahrenheit(6);
		assertFalse("Unsuccessful conversion", celsius == Math.round(-14.51));
	}

}
